$(function($) {

	/*var button = $('.menudropdownMenuButton');
	button.on('click', function(){
		button.fadeOut();
	});*/
	$('.scroll-content section .section-title').on('click', function (event) {

        var sibling = $(this).siblings('.open-content');
        var cover = $(this).parent('.scroll-content-cover');

        if (sibling.hasClass('opened')) {
            $('.open-content.opened').removeClass('opened').slideUp();
            $(this).removeClass('opened');
            $('.scroll-content-cover').removeClass('scroll-content-cover-opened');
        }
        else {
            $('.open-content.opened').removeClass('opened').slideUp();
            $('.scroll-content section .section-title').removeClass('opened');
            $(this).addClass('opened');
            sibling.addClass('opened').slideDown();
            $('.scroll-content-cover').removeClass('scroll-content-cover-opened');
            cover.addClass('scroll-content-cover-opened');
        }
    });

    var previousScroll = 0;
    $(window).scroll(function(event){
        var scroll = $(this).scrollTop(),
            navbar = $("header").first();

        if (scroll > previousScroll && scroll > navbar.height()){
            navbar.filter(':not(:animated)').fadeOut(300);
        } else {
            navbar.filter(':not(:animated)').fadeIn(300);
        }
       previousScroll = scroll;
    });

    /*var scrollup = $('<a>', {
                    href : "#",
                    class : 'scrollup'
                });
    scrollup.hide().appendTo('body');*/

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    if( $( window ).width() >=780 ){
        $(".blogItem").slice(0, 6).css("display", "inline-block");
    }
    else{
      $(".blogItem").slice(0, 3).css("display", "inline-block");
    }
    $("#loadMore").on('click', function (e) {
        e.preventDefault();
        $(".blogItem:hidden").slice(0, 3).slideDown().css("display", "inline-block");
        if ($(".blogItem:hidden").length == 0 /*|| $(".blogItem:hidden").length == 3*/) {
            $("#load").fadeOut('slow');
            $(".blogContent").addClass('loaded');
            $(".blogGrad").css("display", "none");
            $("#loadMore").fadeOut();
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1);
    });

    $('#supportCarouselCover, #productCarousel, #productDemandCarouselCover, #flatHouseCarouselCover').owlCarousel({
      margin: 0,
      nav: true,
      loop: true,
      autoHeight : true,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 1
        },
        760: {
          items: 1
        },
        1000: {
          items: 1
        }
      }
    });

    $('#blogDetailCarousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        autoHeight: true,
        responsive:{
            0:{
                items:1
            }
        }
    });


    /*
     * Typehead
     */
    window.substringMatcher = function(data, column) {
        var chars = [
            'aacdeeillnoooorstuuuuyrzAACDEEILLNOOOORSTUUUUYRZ',
            'áäčďéěíĺľňóôőöŕšťúůűüýřžÁÄČĎÉĚÍĹĽŇÓÔŐÖŔŠŤÚŮŰÜÝŘŽ',
        ];

        var accentedForms = {};
        for ( var i = 0; i < chars[0].length; i++ )
        {
            if ( !(chars[0][i] in accentedForms) )
                accentedForms[chars[0][i]] = chars[1][i];
            else
                accentedForms[chars[0][i]] += chars[1][i];
        }

        return function findMatches(q, cb) {
            if ( typeof data == 'string' )
                strs = !searchData ? [] : searchData[data];
            else
                strs = data;

            for (var s in accentedForms) {
                q = q.replace(new RegExp(s, 'g'), '[' + s + accentedForms[s] + ']');
            }

            var matches, substringRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function(i, str) {
                if (substrRegex.test(str[column])) {
                    matches.push(str);
                }
            });

            cb(matches);
        };
    };

    $('.typeahead-support').typeahead(null, {
      name: 'best-pictures',
      display: 'name',
      source: window.substringMatcher(window.faq_answers, 'name'),
      templates: {
        suggestion: function(obj){
            return '<div class="answer"><strong>'+obj.name+'</strong>'+obj.answer+'</div>';
        },
      }
    });

    if ( 'datepicker' in jQuery.fn )
    {
        !function(a){a.fn.datepicker.dates.cs={days:["Neděle","Pondělí","Úterý","Středa","Čtvrtek","Pátek","Sobota"],daysShort:["Ned","Pon","Úte","Stř","Čtv","Pát","Sob"],daysMin:["Ne","Po","Út","St","Čt","Pá","So"],months:["Leden","Únor","Březen","Duben","Květen","Červen","Červenec","Srpen","Září","Říjen","Listopad","Prosinec"],monthsShort:["Led","Úno","Bře","Dub","Kvě","Čer","Čnc","Srp","Zář","Říj","Lis","Pro"],today:"Dnes",clear:"Vymazat",weekStart:1,format:"dd.m.yyyy"}}(jQuery);

        $('.js-date').datepicker({
            autoclose: true,
            format: 'dd.mm.yyyy',
            language: $('html').attr('lang'),
        });
    }

    $('form.autoAjax').autoAjax();

}(jQuery));