window.autoAjax = {
    bindRow : function(obj){
        /*
         * Bind form values
         */
        var data = obj||$(this).attr('data-row');

        if ( data )
        {
            var data = obj||$.parseJSON( data );

            for ( key in data )
            {
                var input = $(this).find('*[name="'+key+'"]');
                if ( (!data[key] || data[key].length == 0) && (data[key] !== false && data[key] !== 0) )
                {
                    continue;
                }

                autoAjax.bindValue($(this), input, key, data[key]);
            }
        }
    },
    bindValue : function(form, input, key, value){
        if ( input.is('input:radio') || input.is('input:checkbox') )
        {
            if ( value === true )
                value = 1;
            else if ( value === false )
                value = 0;

            input = form.find('*[name="'+key+'"][value="'+value+'"]');
            input.prop("checked", true).change();
        } else {
            if ( value === true )
                value = 1;
            else if ( value === false )
                value = 0;

            input.val(value).change();
        }
    },
    /*
     * Datepicker in form
     */
    bindDatepickers(){
        if ( ! ('datepicker' in jQuery.fn) )
            return;

        $(this).find('.js_date').datepicker({
            autoclose: true,
            format: 'dd.mm.yyyy',
            language: 'sk',
        });
    },
    resetErrors : function(form){
        form.find('.message, .alert').html('').hide();
        form.find('span.error').remove();
        form.find('.has-error').removeClass('has-error');
        form.find('input.error, select.error, textarea.error').removeClass('error');
    },
    setMessage : function(form, message, type){
        form.find('.alert').removeClass('alert-danger alert-success').addClass(type == 'error' ? 'alert-danger' : 'alert-success').html(message).show();
    },
    bindInputError(input, message, autoReset){
        var errorText = ! input.next().hasClass('help-block')
                            ? '<span class="help-block error">'+message+'</span>'
                            : '';

        var error_here = input.parent().find('.error-here');

        error_here.after(errorText);

        input
            .after(error_here.length == 0 ? errorText : '')
            .first().parent()
            .addClass('has-error')
            .one('keyup click', function(){
                if ( autoReset == false )
                    return;

                $(this).removeClass('has-error');
                $(this).find('span.error').remove();
                $(this).find('.error').removeClass('error');
            });
    },
    /*
     * Show modal with callback
     */
    showModal(response){
        $('.modal').modal('hide');

        $('#autoAjaxModal').modal().one('hide.bs.modal', function(){
            if ( 'callback' in response && response.callback )
                eval(response.callback);
            else if ( 'redirect' in response && response.redirect )
                window.location.href = response.redirect;
        }).each(function(){
            $(this).find('.modal-header')[ response.title ? 'show' : 'hide' ]();
            $(this).find('.modal-header .modal-title').html(response.title);
            $(this).find('.modal-body p').html(response.message||response.error);
            $(this).find('button.close').focus();
        })
    },
    fn : function(data){
        $(this).each(function(){

            autoAjax.bindRow.call(this, data);
            autoAjax.bindDatepickers.call(this);

            if ( this._autoAjax === true )
                return;

            this._autoAjax = true;

            var stop = false;

            /*
             * After submit form
             */
            $(this).submit(function(){
                if ( stop == true )
                    return false;

                stop = true;

                var form = $(this);

                autoAjax.resetErrors(form);

                var submit = form.find('*[type="submit"]');

                if ( submit.next().hasClass('hidden') && submit.next().is('button') )
                {
                    submit.toggleClass('hidden');
                    submit.next().toggleClass('hidden');
                }

                form.ajaxSubmit({
                  url: form.attr('data-action'),
                  success: function(response){
                    autoAjax.resetErrors(form);

                    stop = false;

                    if ( form.hasClass('autoReset') && !('error' in response) )
                        form.find('input, select, textarea').not('input[name="_token"], input[type="submit"], input[type="hidden"]').val('');

                    autoAjax.ajaxResponse(response, form);
                  },
                  error: function(response){
                    autoAjax.resetErrors(form);

                    stop = false;

                    if ( response.status == 422 || response.status == 403 ){

                        var obj = response.responseJSON;

                        if ( response.status == 422 )
                        {

                            //Laravel 5.5 provides validation errors in errors object.
                            if ( 'errors' in obj && !('length' in obj.errors) )
                                obj = obj.errors;

                            for ( var key in obj )
                            {
                                var message = $.isArray(obj[key]) ? obj[key][0] : obj[key],
                                    input = form.find('input[name="'+key+'"], select[name="'+key+'"], textarea[name="'+key+'"]');

                                autoAjax.bindInputError(input, message);
                            }

                            autoAjax.setMessage(form, obj.message||'Vyplňte prosím všechny potřebné položky správně.', 'error');
                        }
                    } else {
                        autoAjax.setMessage(form, obj.message||'Nastala neočekávaná chyba, zkuste později prosím.', 'error');
                    }
                  }
                });

                return false;
            });
        })
    },
    /*
     * Return correct ajax response
     */
    ajaxResponse(response, form)
    {
        //If form has own response callback
        if ( form && form.attr('data-callback') )
            return window[form.attr('data-callback')].call(null, response);

        if ( 'error' in response || 'message' in response )
        {
            if ( response.type == 'modal' )
            {
                autoAjax.showModal(response);
            } else {
                autoAjax.setMessage(form , response.message, 'message' in response ? 'success' : 'error');

                if ( 'callback' in response && 'error' in response )
                    eval(response.callback);
            }

            return true;
        } else if ( 'redirect' in response )
        {
            if ( response.redirect == window.location.href )
                return window.location.reload();

            window.location.href = response.redirect;

            return true;
        }

        return false;
    },
    unknownError()
    {
       autoAjax.showModal({
            message : window.trans.unknown_error
       });
    },
}

jQuery.fn.autoAjax = autoAjax.fn;