const gulp        = require('gulp');
const browserSync = require('browser-sync').create();
const sass        = require('gulp-sass');

// Compile Sass & Inject Into Browser
gulp.task('sass', function() {
    return gulp.src(['./sass/*.scss'])
        .pipe(sass())
        .pipe(gulp.dest("./assets/css/"))
        .pipe(browserSync.stream());
});


// Watch Sass & Serve
gulp.task('serve', ['sass'], function() {
    browserSync.init({
        server: "."
    });

    gulp.watch(['./sass/**/*.scss'], ['sass']); // zober vsetky scss zo vsadial **
    gulp.watch("*.html").on('change', browserSync.reload);
});

// Default Task
gulp.task('default', ['serve']);


// npm install gulp-sass --save-dev
// npm install browser-sync --save-dev